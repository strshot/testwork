<?php
namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Lead;
use App\Models\SphereMask;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(){
	    return view('views.page.index');
    }
    //
    public function test(){
        $dataArray = Lead::has('obtainedBy')->get();
        return view('views.page.test',['dataArray'=>$dataArray]);
    }

    public function testAjax(){
        $id = $_GET['id'];
        $data = Lead::has('obtainedBy')->find($id);
        $arr[] = ['date',$data->date];
        $arr[] = ['name',$data->name];
        $arr[] = ['phone',$data->phone->phone];
        $arr[] = ['email',$data->email];
        foreach ($data->sphereAttr as $key=>$sphereAttr){
            $str = '';
            foreach ($sphereAttr->options as $option){
                $mask = new SphereMask($data->sphere_id,$data->id);
                $resp = $mask->where('type','lead')->where('fb_'.$option->sphere_attr_id.'_'.$option->id,1)->get()->toArray();
                if (count($resp))
                    $str .= $option->value;
            }
            $arr[] = [$sphereAttr->label,$str];
        }
        echo json_encode(['data'=>$arr]);exit;
    }
}
